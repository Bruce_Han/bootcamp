package com.bootcamp.assignment.service;

import com.bootcamp.assignment.domain.ExcelDAO;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.session.SqlSession;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Mapper
public class ExcelServiceImpl implements ExcelService {

    SqlSession sqlSession;
    public ExcelServiceImpl(SqlSession sqlSession) {
        this.sqlSession = sqlSession;
    }

    private static String namespace = "com.bootcamp.assignment.mybatis.mapper.ExcelMapper";

    @Override
    public int create(ExcelDAO excelDAO) {
        int count = 0;
        count = sqlSession.insert(namespace + ".create", excelDAO);
        return count;
    }

    @Override
    public ExcelDAO readById(ExcelDAO excelDAO) {
        return sqlSession.selectOne(namespace + ".read", excelDAO.getSeq());
    }

    @Override
    public List<ExcelDAO> readAll() {
        return sqlSession.selectOne(namespace + ".readList");
    }

    @Override
    public int update(ExcelDAO excelDAO) {
        int count = 0;
        count = sqlSession.insert(namespace + ".update", excelDAO);
        return count;
    }

    @Override
    public int delete(ExcelDAO excelDAO) {
        int count = 0;
        count = sqlSession.insert(namespace + ".delete", excelDAO.getSeq());
        return count;
    }
}
