package com.bootcamp.assignment.domain;

import lombok.Data;

@Data
public class ExcelDAO {
    private Long seq;
    private String workCode;
    private String workName;
    private String company;
    private String manager;
    private String rank;
    private String category;
    private String phone;
}
