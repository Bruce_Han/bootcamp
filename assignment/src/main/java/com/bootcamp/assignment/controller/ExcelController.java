package com.bootcamp.assignment.controller;

import com.bootcamp.assignment.domain.ExcelDAO;
import com.bootcamp.assignment.service.ExcelService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/excels")
public class ExcelController {

    final ExcelService excelService;

    public ExcelController(ExcelService excelService) {
        this.excelService = excelService;
    }

    @GetMapping("/")
    public String index() {
        return "redirect:/dataList";
    }

    @GetMapping("/inputForm")
    public String inputForm() {
        return "/excels/inputForm";
    }

    @PostMapping("")
    public String inputData(HttpServletRequest request) {
        ExcelDAO excelDAO = new ExcelDAO();
        excelDAO.setWorkCode(request.getParameter("workCode"));
        excelDAO.setWorkName(request.getParameter("workName"));
        excelDAO.setCompany(request.getParameter("company"));
        excelDAO.setManager(request.getParameter("manager"));
        excelDAO.setRank(request.getParameter("rank"));
        excelDAO.setCategory(request.getParameter("category"));
        excelDAO.setPhone(request.getParameter("phone"));
        excelService.create(excelDAO);

        return "redirect:/dataList";
    }

    @GetMapping("/{id}")
    public String seeDataView(@PathVariable("seq") Long seq, Model model) {
//        model.addAttribute("data", excelService.readById(seq));
        return "/excels/dataView";
    }

    @GetMapping("/dataList")
    public String seeDataList() {
        return "/excels/dataList";
    }

    @PutMapping("")
    public String updateData() {
        return "redirect:/dataList";
    }

    @DeleteMapping("")
    public String deleteData() {
        return "redirect:/dataList";
    }


}
