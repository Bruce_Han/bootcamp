package com.bootcamp.assignment.service;

import com.bootcamp.assignment.domain.ExcelDAO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ExcelService {
    int create(ExcelDAO excelDAO);
    ExcelDAO readById(ExcelDAO excelDAO);
    List<ExcelDAO> readAll();
    int update(ExcelDAO excelDAO);
    int delete(ExcelDAO excelDAO);
}
